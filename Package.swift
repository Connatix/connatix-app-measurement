// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let version = "1.5.3"
let checksum = "6e9819929ee9838fa3ec2791ffefa4c60bf0cd16c92ebcf164b70a348dcee621"

let package = Package(
    name: "connatix-app-measurement",
    platforms: [.iOS(.v12)],
    products: [
        .library(
            name: "ConnatixAppMeasurement",
            targets: ["OMSDK_Connatix"]),
    ],
    targets: [
        .binaryTarget(name: "OMSDK_Connatix",
                      url: "https://gitlab.com/Connatix/connatix-app-measurement/-/raw/\(version)/OMSDK_Connatix.zip",
                      checksum: checksum)
    ]
)
