# connatix-app-measurement

Special repository for Swift Package Manager integration.

Peer dependency for `connatix-player-sdk` and `connatix-player-sdk-objc`.

Should not be used as a standalone package.

Please consult the official Connatix support documentation for further information.
